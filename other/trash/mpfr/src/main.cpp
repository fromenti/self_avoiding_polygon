#include "coefficients.hpp"
#include "polygon.hpp"

int main(){
  try{
    Polygon P("u100l100d100r100");
    mpfr_printf("%Re\n",P.get_fp());
  }
  catch(const Error& error){
    cerr<<error.msg<<endl;
  }


}
