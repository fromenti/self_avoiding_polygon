#ifndef ADJACENCY_MATRIX_HPP
#define ADJACENCY_MATRIX_HPP

#include <iostream>

using namespace std;

class AdjacencyMatrix{
protected:
  size_t n;
  char* data;
public:
  AdjacencyMatrix();
  ~AdjacencyMatrix();
  void init(size_t n);
  void clear();
  int get(size_t i,size_t j) const;
  void set(size_t i,size_t j);
  unsigned int get_diag_square_sym(size_t i) const;
};

inline
AdjacencyMatrix::AdjacencyMatrix(){
  n=0;
  data=nullptr;
}

inline
AdjacencyMatrix::~AdjacencyMatrix(){
  if(data!=nullptr) delete[] data;
}


inline void
AdjacencyMatrix::clear(){
  for(size_t i=0;i<n*n;++i){
    data[i]=0;
  }
}

inline int
AdjacencyMatrix::get(size_t i,size_t j) const{
  return data[i*n+j];
}

inline void
AdjacencyMatrix::set(size_t i,size_t j){
  data[i*n+j]=1;
}

#endif
