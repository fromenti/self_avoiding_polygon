#ifndef POLYGON_HPP
#define POLYGON_HPP


#include <iostream>
#include <string>
#include <vector>
#include "vertex.hpp"
#include "matrix.hpp"
#include "adjacency_matrix.hpp"
#include "coefficients.hpp"
#include "error.hpp"

using namespace std;

class Polygon{
private:
  static Coefficients coefficients;
  size_t length;
  vector<Vertex> vertices;
  unordered_map<Vertex,uint64> indices;
  AdjacencyMatrix B;
  mpfr_t fp;
  void compute_B();
  void compute_C(Matrix& C);
  void compute_M(Matrix& M,const Matrix& C);
  void compute_fp();
  void add_vertex(const int32& x,const int32& y);
  void add_neighbours(const int32& x,const int32& y);
public:
  static Coefficients& get_coefficients();
  Polygon();
  Polygon(string str);
  size_t size() const;
  size_t graph_size() const;
  Vertex vertex(size_t i) const;
  int get_coeff_B(size_t i,size_t j) const;
  mpfr_srcptr get_fp() const;

};

inline
Polygon::Polygon(){
  length=0;
}

inline size_t
Polygon::size() const{
  return length;
}

inline size_t
Polygon::graph_size() const{
  return vertices.size();
}

inline Vertex
Polygon::vertex(size_t i) const{
  return vertices[i];
}

inline Coefficients&
Polygon::get_coefficients(){
  return coefficients;
}

inline  mpfr_srcptr
Polygon::get_fp() const{
  return fp;
}

inline int
Polygon::get_coeff_B(size_t i,size_t j) const{
  return B.get(i,j);
}

#endif
