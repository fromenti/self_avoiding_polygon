#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <iostream>
#include <immintrin.h>
#include <mpfr.h>
#include "config.hpp"

using namespace std;

class Matrix{
protected:
  size_t nr,nc;
  mpfr_t* data;
public:
  Matrix();
  ~Matrix();
  void init(size_t n);
  void init(size_t nrow,size_t ncol);
  mpfr_srcptr get(size_t i,size_t j) const;
  mpfr_ptr get(size_t i,size_t j);
  void set(size_t i,size_t j,mpfr_t x);
  void clear();
  void display();

  void swap_lines(size_t i,size_t j);
  void mul_line(size_t i,mpfr_t a);
  void add_mul_line(size_t i,size_t j,mpfr_t a);
  void Gauss(mpfr_t det);
};


//******************
//* Inline methods *
//******************

inline
Matrix::Matrix(){
  nr=0;
  nc=0;
  data=nullptr;
}

inline void
Matrix::init(size_t n){
  return init(n,n);
}

inline mpfr_srcptr
Matrix::get(size_t i,size_t j) const{
  return data[i*nc+j];
}

inline mpfr_ptr
Matrix::get(size_t i,size_t j){
  return data[i*nc+j];
}

inline void
Matrix::set(size_t i,size_t j,mpfr_t x){
  mpfr_set(data[i*nc+j],x,MPFR_RNDN);
}

#endif
