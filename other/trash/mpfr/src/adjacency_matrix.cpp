#include "adjacency_matrix.hpp"

void
AdjacencyMatrix::init(size_t _n){
  n=_n;
  if(data!=nullptr) delete[] data;
  data=new char[n*n];
}

unsigned int
AdjacencyMatrix::get_diag_square_sym(size_t i) const{
  unsigned int res=0;
  for(size_t j=0;j<n;++j){
    res+=get(i,j);
  }
  return res;
}
