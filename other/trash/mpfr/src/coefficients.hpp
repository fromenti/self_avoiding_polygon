#ifndef COEFFICIENTS_HPP
#define COEFFICIENTS_HPP

#include <mpfr.h>
#include "config.hpp"


class Coefficients{
private:

  static constexpr size_t imax=1000;
  static constexpr size_t ncoeffs=((imax+2)*(imax+1))/2;
  mpfr_t* coeffs;
  size_t pos(size_t i,size_t j);
public:
  Coefficients();
  void set(mpfr_t c,size_t i,size_t j);
  mpfr_ptr get(size_t i,size_t j);

};

inline size_t
Coefficients::pos(size_t i,size_t j){
  return i+j*(j+1)/2;
}

inline void
Coefficients::set(mpfr_t c,size_t i,size_t j){
  mpfr_set(c,(i<j)?coeffs[pos(i,j)]:coeffs[pos(j,i)],MPFR_RNDN);
}

inline mpfr_ptr
Coefficients::get(size_t i,size_t j){
  return(i<j)?coeffs[pos(i,j)]:coeffs[pos(j,i)];
}
#endif
