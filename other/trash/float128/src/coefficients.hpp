#ifndef COEFFICIENTS_HPP
#define COEFFICIENTS_HPP

#include <mpfr.h>

using Reel = __float128;

class Coefficients{
private:
  static constexpr size_t prec=4096;
  static constexpr size_t imax=1000;
  static constexpr size_t ncoeffs=((imax+2)*(imax+1))/2;
  Reel coeffs[ncoeffs];
  size_t pos(size_t i,size_t j);
public:
  Coefficients();
  Reel get(size_t i,size_t j);
};

inline size_t
Coefficients::pos(size_t i,size_t j){
  return i+j*(j+1)/2;
}

inline Reel
Coefficients::get(size_t i,size_t j){
  return (i<j)?coeffs[pos(i,j)]:coeffs[pos(j,i)];
}

#endif
