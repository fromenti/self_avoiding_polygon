#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <iostream>
#include <immintrin.h>

using namespace std;

using Reel = __float128;

class Matrix{
protected:
  size_t nr,nc;
  Reel* data;
public:
  Matrix();
  void init(size_t n);
  void init(size_t nrow,size_t ncol);
  Reel get(size_t i,size_t j) const;
  Reel& get(size_t i,size_t j);
  void clear();
  void display() const;

  void swap_lines(size_t i,size_t j);
  void mul_line(size_t i,Reel a);
  void add_mul_line(size_t i,size_t j,Reel a);
  Reel get_diag_square_sym(size_t i) const;
  Reel Gauss();
};

//******************
//* Inline methods *
//******************

inline
Matrix::Matrix(){
  nr=0;
  nc=0;
  data=nullptr;
}

inline void
Matrix::init(size_t n){
  return init(n,n);
}

inline Reel
Matrix::get(size_t i,size_t j) const{
  return data[i*nc+j];
}

inline Reel&
Matrix::get(size_t i,size_t j){
  return data[i*nc+j];
}


#endif
