#include "error.hpp"

void
Error::string_error(string msg,string str,size_t pos){
  Error error;
  error.msg=msg+" : ";
  for(size_t i=0;i<min(pos,str.size());++i) error.msg+=str[i];
  error.msg+="\033[35m";
  error.msg+=str[pos];
  error.msg+="\033[0m";
  for(size_t i=pos+1;i<str.size();++i) error.msg+=str[i];
  throw(error);
}

void
Error::error(string msg){
  Error error;
  error.msg=msg;
  throw(error);
}
