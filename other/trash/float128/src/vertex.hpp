#ifndef VERTEX_HPP
#define VERTEX_HPP

#include <cstdint>
#include <unordered_map>

using namespace std;

using int32=int32_t;
using uint32=uint32_t;
using uint64=uint64_t;
using int64=int64_t;

class Vertex{
public:
  int32 x,y;
  Vertex();
  Vertex(int x,int y);
  Vertex(const Vertex& v);
  bool operator==(const Vertex& v);
 };

inline
Vertex::Vertex(){
}

inline
Vertex::Vertex(int32 _x,int32 _y):x(_x),y(_y){
}

inline Vertex::Vertex(const Vertex& v){
  x=v.x;
  y=v.y;
}

template<>
struct std::hash<Vertex>{
  size_t operator()(const Vertex& v) const;
};

template<>
struct std::equal_to<Vertex>{
  constexpr bool operator()(const Vertex& u,const Vertex& v) const;
};


inline size_t
std::hash<Vertex>::operator()(const Vertex& v) const{
  uint64 x=(uint32)v.x;
  uint64 y=(uint32)v.y;
  return (x<<32)+y;
}

inline constexpr bool
std::equal_to<Vertex>::operator()(const Vertex& u,const Vertex& v) const{
  return u.x==v.x and u.y==v.y;
}

#endif
