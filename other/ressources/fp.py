var('tau')

def C_int(dx,dy):
    return -1/pi*integral(1/tau*(1-((tau-I)/(tau+I))**(dx-dy)*((tau-1)/(tau+1))**(dx+dy)),tau,0,infinity)
    
def C_diag(m):
   return -2/pi*(harmonic_number((2*m-1)/2)+log(4))

var('c1','c2','c3','c4','c5')
var('x')
l=[0,c1,c2,c3,c4,c5]
l=[0,-4*x,-4*x*(1+1/3),-4*x*(1+1/3+1/5),-4*x*(1+1/3+1/5+1/7)]

def c(i,j):
    if i>j:
        return c(j,i)
    if (i,j)==(0,1):
        return -1
    if i==j:
        return l[i]
    if i==0:
        return 4*c(0,j-1)-c(0,j-2)-2*c(1,j-1)
    if i==j-1:
        return 2*c(j-1,j-1)-2*c(j-2,j)
    return 4*c(i,j-1)-c(i,j-2)-c(i-1,j-1)-c(i+1,j-1)

var('k')

def d(i,j):
    if i>j:
        return d(j,i)
    r=1
    for k in range(j):
        r=r*(2*k+1)
    return r*c(i,j)
