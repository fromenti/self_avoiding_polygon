import fp
C=fp.Coefficients()

def erreur():
    L=flatten([[C.get(i,j) for i in range(j+1)] for j in range(1001)])
    R=flatten(load('65536.txt.sobj'))
    E=set([abs(L[i]-R[i]) for i in range(len(L))])
    return max(E)

def surface(imax=1000):
    P=[]
    for j in range(imax+1):
        for i in range(j+1):
            P.append((i,j,exp(-C.get(i,j))))
    show(list_plot3d(P,interpolation_type='linear'))
