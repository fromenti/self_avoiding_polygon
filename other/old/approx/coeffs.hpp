#ifndef COEFFS_HPP
#define COEFFS_HPP

#include "config.hpp"
#include "rationnal.hpp"

//**********************
//* Coefficients C_m,n *
//**********************

//! Maximal coefficinet index
static const size_t max_ind_coeffs=max_len/2+2;

//! Number of coefficients
static const size_t num_coeffs=((max_ind_coeffs+1)*(max_ind_coeffs+2))/2;

//! Array of coefficients
extern Reel coeffs[num_coeffs];

//! Return the indice of coefficient C_{i,j}
size_t pos(size_t i,size_t j);

//! Return an approximation of coefficient C_{i,j}
Reel get_coeff(size_t i,size_t j);

//! Compute all the coefficients
void compute_coeffs();

//! Represent an analytic coefficient
struct CoeffAnalytic{
  //! The coefficient is a-4b/pi
  Rationnal a,b;
};

//********************
//* Inline functions *
//********************

inline size_t
pos(size_t i,size_t j){
  return (j*(j+1))/2+i;
}

inline Reel
get_coeff(size_t i,size_t j){
  return i<j?coeffs[pos(i,j)]:coeffs[pos(j,i)];
}

#endif
