#ifndef CONFIG_HPP
#define CONFIG_HPP

#include <iostream>

using namespace std;

//! Maximal self avoiding polygon length to consider
static const size_t max_len=20; // Must be less or equal tin [6,38]

typedef double Reel;
typedef __int128 Int;
typedef int64_t int64;

ostream& operator<<(ostream& os,const Int& x);

//********************
//* Inline functions *
//********************

inline ostream&
operator<<(ostream& os,const __int128& x){
  if(x==0) return os<<"0";
  string str;
  __int128 ax=(x<0)?-x:x;
  while(ax!=0){
    str=to_string((int)ax%10)+str;
    ax=ax/10;
  }
  if(x<0) os<<'-';
  return os<<str;
}

#endif

