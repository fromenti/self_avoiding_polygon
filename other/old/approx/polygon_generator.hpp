#ifndef POLYGON_GENERATOR_HPP
#define POLYGON_GENRATOR_HPP

#include "polygon.hpp"

//********************
//* PolygonGenerator *
//********************

//! Generates all self avoidinfg polygon upto length max_len.
//! The class contains a unique instance of a polygon. We use
//! a stack to store each Step of the exploration.

class PolygonGenerator{
private:
  //! A stack of Step
  stack<Step> s;

  //! The unique polygon
  Polygon poly;

public:
  //! The id of the next step
  size_t n;

  //! Empty constructor
  PolygonGenerator();

  //! Explore until the next self avoiding polygon
  //! Return is such a polygon exists and false otherwise.
  bool next();

  //! Return a reference to the current self avoiding polygon.
  const Polygon& get() const;

  //! Set the current self avoiding polygon to P
  void set(Polygon& P) const;
};

//********************
//* Inline functions *
//********************

inline
PolygonGenerator::PolygonGenerator(){
  n=1;
  poly.init();
  s.emplace(2,poly.head,Polygon::right(poly.head),n);
}

inline const Polygon&
PolygonGenerator::get() const{
  return poly;
}

inline void
PolygonGenerator::set(Polygon& P) const{
  P=poly;
}
#endif

