#include <Python.h>

#include "error.hpp"


void raise_py_error(){
  try{
    throw;
  } catch(Error& error){
    PyErr_SetString(PyExc_RuntimeError,error.msg.c_str());
  }

}
