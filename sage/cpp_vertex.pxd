from libc.stdint cimport int32_t

ctypedef int32_t int32

cdef extern from "vertex.hpp":
  cdef cppclass cpp_Vertex "Vertex":
    int32 x
    int32 y
