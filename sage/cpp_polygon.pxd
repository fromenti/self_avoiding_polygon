from libcpp.string cimport string
from cpp_vertex cimport cpp_Vertex
from cpp_coefficients cimport cpp_Coefficients
cdef extern from "polygon.cpp":
  pass

cdef extern from "matrix.hpp":
  pass

cdef extern from "matrix.cpp":
  pass

cdef extern from "error.hpp":
  pass

cdef extern from "error.cpp":
  pass

cdef extern from "py_error.hpp":
  cdef int raise_py_error()

cdef extern from "polygon.hpp":
  cdef cppclass cpp_Polygon "Polygon":
    cpp_Polygon()
    cpp_Polygon(string) except +raise_py_error
    size_t size()
    size_t graph_size()
    cpp_Vertex vertex(size_t i)
    double get_coeff_B(size_t i,size_t j)
    double get_coeff_C(size_t i,size_t j)
    double get_coeff_M(size_t i,size_t j)
    double get_fp()
    @staticmethod
    cpp_Coefficients get_coefficients()

#cdef extern from "polygon.cpp" namespace "Polygon":
#  cpp_Coefficients get_coefficients()
