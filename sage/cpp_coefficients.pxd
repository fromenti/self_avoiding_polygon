cdef extern from "coefficients.cpp":
  pass

cdef extern from "coefficients.hpp":
  cdef cppclass cpp_Coefficients "Coefficients":
    cpp_Coefficients()
    double get(size_t i,size_t j)
