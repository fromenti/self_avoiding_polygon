from cpp_coefficients cimport cpp_Coefficients

cdef class Coefficients:
  cdef cpp_Coefficients cpp

  def __cinit__(self,init=True):
    if init:
      self.cpp=cpp_Coefficients()

  def get(self,i,j):
    return self.cpp.get(i,j)
