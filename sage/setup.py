from setuptools import Extension,setup
from Cython.Build import cythonize

extensions = [
  Extension("fp",
    sources=["sage/fp.pyx"],
    include_dirs=["src"],
    libraries=[],
    library_dirs=[],
    extra_compile_args = ['-O3','-mavx2','-mfma'],
  )
]

setup(ext_modules=cythonize(extensions))
