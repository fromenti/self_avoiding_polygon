CPP		= g++
CPPFLAGS	= -O3 -mavx2 -mfma -g
EXE		= fp
LIB		= -lmpfr
SAGE		= sage
PYTHON 		= $(SAGE) -python
MODULE		= fp.so

all: $(EXE) sage

sage: $(MODULE)


$(EXE): src/*.hpp src/*.cpp
	$(CPP) $(CPPFLAGS) $^ -o $@ $(LIB)

$(MODULE): sage/setup.py sage/fp.pyx
	$(PYTHON) sage/setup.py build_ext --inplace

clean:
	$(RM) -r build *.so sage/*.cpp $(EXE)
